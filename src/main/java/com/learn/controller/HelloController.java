package com.learn.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: Holoong
 * @Date: 2020/10/14 19:04
 */
@RestController
public class HelloController {

    //http://localhost:9080/hello
    @GetMapping("/hello")
    public String hello(){

        return "何龙";
    }
}

